// tester avec postman
const feathers = require('@feathersjs/feathers')
const express = require('@feathersjs/express')
const service = require('feathers-knex')
const knex = require('knex')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

let saltRounds = 10
let config = require('./knexfile.js')
let database = knex(config.development)
//let privateKey = fs.readFileSync('private.key');
//let cert = fs.readFileSync('public.pem');  // get public key


// Create a feathers instance.
const app = express(feathers())
// Turn on JSON parser for REST services
app.use(express.json())
// Turn on URL-encoded parser for REST services
app.use(express.urlencoded({ extended: true }))
// Enable REST services
app.configure(express.rest());
//Verify indentity
app.post("/create-account",async function (request, response) {
  let data = request.body
  let name = data.fullname
  let newEmail = data.email
  async function newUser() {
    try { //to do, il faut que ça marche
      await database.into('User').insert({fullname: name, email:newEmail})
    }
    catch {
      response.redirect("./user_exists.html")
    }
 }
 newUser()
  // userId = trouver le id dans la db et le mettre dans la signature du token 
  jwt.sign({ fullname: name, email: newEmail }, 
    "super_secret", 
    function(err, token) {
    console.log(`Veuillez clicker sur localhost:3000/confirm-registration?token=${token}`);
  });
  
  
  response.redirect("./confirmation.html")
}) 
  
app.get("/confirm-registration", async function (request, response) {
  let token = request.query.token
  userMail= jwt.decode(token).email
  userName= jwt.decode(token).fullname
  console.log(userMail)
  try {
  let decoded = jwt.verify(token, "super_secret")
  //to do : confirmer user avec database
    response.send(
      `<body>
        <form  method="POST" action ="/activate-account">
          Mot de passe :<br/>
          <input type="password" name="password" placeholder="Entrez votre mot de passe"><br/>
          <input type="hidden" name="email" value="${userMail}"> 
          <input type="submit" value="Envoyer" >
        </form>
        
      </body>`

    )
    cosole.log
   }
   catch {
     response.redirect("./invalid-token.html") //to do : ca ne va pas marcher, le mail est deja dans la db
   }
  }
)
app.post("/activate-account", async function (request, response) {
  // hash pw and add it to the db
  let hash = bcrypt.hashSync(request.body.password, saltRounds)
  console.log(`hash: ${hash}, pass: ${request.body.password} email : ${request.body.email}`)
  // send to login 
   //userdId = database.select('id').from<User>()
  await database.into('User').update({active: true , password: hash}).where({email: request.body.email})

  console.log(`je marche toujours `)
  response.redirect("./login.html")
})

app.post("/login", async function (request, response) {
  
  let users = await database.select('*').from('User').where({email: request.body.email})
  if (users.length === 0) {
    response.send(`
    <body>
    <p>invalid user email</p>
    </body>
    `)
  } else {
      if (bcrypt.compareSync(request.body.password , users[0].password)){
      response.redirect("./home.html")
      } else {
      response.redirect("./invalidUser.html")
       }
    }
}
)

app.use('/api/users', service({
  Model: database,
  name: 'User'
}))

app.use('/api/pictures', service({
  Model: database,
  name: 'Picture'
}))
app.use(express.static('./static'))
app.use(express.errorHandler())

// Start the server.
const port = process.env.PORT || 3000

app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})